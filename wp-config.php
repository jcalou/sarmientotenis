<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'sarmientotenis');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', '127.0.0.1');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'nR[;*K#c&a`Jl^rwy+Xhi|Ua Wh;jZwFJ6GfmH2vP$}s#-J/5!/O>]/$uoipP`q?');
define('SECURE_AUTH_KEY',  ':~pr&y=bTqI[4x63}Kos1U*(5Fq,HDkePvL/^RZmA}o`?G~<onwO`~+S5QxH+aCP');
define('LOGGED_IN_KEY',    '-tj@XW8&-B)b#bH;w@7Cb6,6VV%<XXF#Yb&xj9im91^.f[#TY5QsX3A7O+IM@lgF');
define('NONCE_KEY',        '+wq=]^u51~JF5;irT5cOEeMF 1hkf.>H#{M?s1!4n+BU+8V[dF`B{-p>P-(Nq-{M');
define('AUTH_SALT',        'i^Gziq.t]AJ[Vv$nAKGG^2O2^]/ d|[;)<Z){0%o3O-wt!hYgTE3J$2}2Yd;RBO@');
define('SECURE_AUTH_SALT', 'AH=@~VV$6$=#KB!z-=X.dD:-+v*A/*w}*0R7U L(2S;y)7bTX7Lsew:nH3p]OeI&');
define('LOGGED_IN_SALT',   'E:^[I&LQ1v!B)IM~`Qj]QfT5f+Qh%OGm|%QU-,ev84`owZ?lHKkC&#*0c}@]e(fY');
define('NONCE_SALT',       ';x%m0: f5i(+|_[>PtvrQ,D^>z%~U!8]0]]8ehL^bjA7j|Imxc^_V;Vl9|7fA,EV');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
