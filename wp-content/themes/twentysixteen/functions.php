<?php

require get_template_directory() . '/inc/environment.php';

function twentysixteen_setup() {

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'twentysixteen' ),
		'social'  => __( 'Social Links Menu', 'twentysixteen' ),
	) );

	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, icons, and column width.
	 */
	add_editor_style( array( 'assets/buils/editor-style.css' ) );
}
add_action( 'after_setup_theme', 'twentysixteen_setup' );

