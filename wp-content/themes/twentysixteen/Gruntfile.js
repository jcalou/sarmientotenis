var path = require('path');
var base = 'assets/';

module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		sass: {
	    dev: {
		  	options: {
			    style: 'expanded',
			    loadPath: require('node-bourbon').includePaths
			  },
				expand: true,
				cwd: base + 'sass',
		  	src: '*.scss',
		    dest: base + 'build',
				ext: '.css'
			},
			dist: {
        options: {
          style: 'compressed',
					sourcemap: 'none',
					loadPath: require('node-bourbon').includePaths
        },
				expand: true,
				cwd: base + 'sass',
      	src: '*.scss',
      	dest: base + 'build',
				ext: '.min.css'
			}
		},

		manifest: {
			options: {
				banner: true
			},
			dist: {
				src: base + 'js',
				dest: base + 'build'
			}
		},

    copy: {
      vendor: {
        src: base + 'build/vendor.js',
        dest: base + 'build/vendor.min.js'
      },
			scripts: {
				expand: true,
				cwd: base + 'js/',
				src: '*.js',
				dest: base + 'build/'
			}
    },

		uglify: {
		  all: {
		    options: {
		      sourceMap: false
		    },
		    src: base + 'build/all.js',
		    dest: base + 'build/all.min.js'
		  },
			scripts: {
		    options: {
		      sourceMap: false
		    },
				expand: true,
		    cwd: base + 'js/',
		    src: '*.js',
		    dest: base + 'build/',
				rename: function(destBase, destPath) {
        	return destBase + destPath.replace('.js', '.min.js');
        }
		  }
		},

		watch: {
			options: {
				atBegin: true
			},
			css: {
				files: base + 'sass/**/*',
				tasks: ['sass:dev']
			},
      js_scripts: {
				files: base + 'js/*.js',
				tasks: ['copy:scripts']
			},
			js_manifests: {
				files: [
					base + 'js/*.json',
					base + 'js/*/**/*'
				],
				tasks: ['manifest']
			}
		}
	});

	// Load all tasks before changing the working path

	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-copy');
	// grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-manifest-concat');

	// Tasks definition

	grunt.registerTask('develop', ['watch']);
	grunt.registerTask('default', ['develop']);
  grunt.registerTask('build', [
    'sass:dist',
    'manifest',
    'uglify:all',
		'uglify:scripts',
    'copy:vendor',
  ]);
};
