<?

function in_production_mode() {
	return 'production' === get_option('environment');
}

function add_leading_slash($string) {
	return '/' . ltrim($string, '/\\');
}

function theme_url($path) {
	return get_template_directory_uri() . add_leading_slash($path);
}

function asset_url($path) {
	$path = str_replace('.min', '', $path);
	$path = in_production_mode() ? preg_replace('/(\.(?:css|js))$/', '.min$1', $path) : $path;
	return theme_url('/assets' . add_leading_slash($path));
}

function add_extension($file, $ext) {
	return rtrim($file, $ext) . $ext;
}

function stylesheet_link_tag($files) {
	foreach ($files as $file) {
		$file = add_extension($file, '.css');
		?><link rel="stylesheet" href="<?= asset_url("/build/{$file}") ?>" media="all"><?php
	}
}

function javascript_link_tag($files) {
	foreach ($files as $file) {
		$file = add_extension($file, '.js');
		?><script src="<?= asset_url("/build/{$file}") ?>" charset="utf-8"></script><?php
	}
}
